﻿namespace Firebase_Try
{
    internal class Data
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Age { get; set; }
        public string Adress { get; set; }
    }
}