﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using FireSharp.Config;
using FireSharp.Interfaces;
using FireSharp.Response;

namespace Firebase_Try
{
    public partial class Form1 : Form
    {
        IFirebaseConfig config = new FirebaseConfig
        {
            AuthSecret = "WMZC9oaHVt7T9lwtyutMrhDrREgyZ3mDDmZl0HDW",
            BasePath = "https://ctry-ee061.firebaseio.com/"
        };
        IFirebaseClient client;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            client = new FireSharp.FirebaseClient(config);
            if(client != null)
            {
                MessageBox.Show("Connection Established");
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private async void button1_Click(object sender, EventArgs e)
        {
            var data = new Data
            {
                Id = textBox2.Text,
                Name = textBox1.Text,
                Age = textBox3.Text,
                Adress = textBox4.Text
            };
            FirebaseResponse response = await client.SetAsync("information/" + textBox2.Text, data);
            //SetResponse response = await client.SetTaskAsync("information/"+textBox2.Text,data);
            Data result = response.ResultAs<Data>();

            MessageBox.Show("Data inserted of ID" + result.Id);
        }

        private async void button2_Click(object sender, EventArgs e)
        {
            FirebaseResponse response = await client.GetAsync("information/" + textBox2.Text);
            if(response.Body != "null")
            {
                Data obj = response.ResultAs<Data>();
                textBox1.Text = obj.Name;
                textBox3.Text = obj.Age;
                textBox4.Text = obj.Adress;
                MessageBox.Show("Data Retrived of ID" + textBox2.Text);
            }
            else
            {
                MessageBox.Show("No data of ID" + textBox2.Text);
            }
           
        }

        private async void button3_Click(object sender, EventArgs e)
        {
            FirebaseResponse response = await client.DeleteAsync("information/"+ textBox2.Text);
            MessageBox.Show("Data deleted of ID " + textBox2.Text);
        }
    }
}
