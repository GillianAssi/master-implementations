#include <iostream>
#include <string>
// Winsock 2nd version --> Windows uses to create sockets
#include <WS2tcpip.h> 

// Meant for the linker, this is the same as adding the lib in the project properties at Linker->Input->Additional dependencies
#pragma comment(lib, "ws2_32.lib")

// Is not great as a global var
using namespace std;

// SOCKETS
SOCKET sock, client;
bool start = false;

void HandleClean(int s)
{
	if (sock)
		closesocket(sock);
	if (client)
		closesocket(client);
	WSACleanup();
	Sleep(1000);
	cout << "Exit Err#" << s;
	exit(0);
}

void CleanSockets(const char *a, int x)
{
	cout << a;
	HandleClean(x + 1000);
}

DWORD WINAPI ReadingThread(LPVOID param)
{
	SOCKET s = (SOCKET)param;
	char buf[4096];
	ZeroMemory(buf, 4096);
	int bytesReceived;

	do
	{
		bytesReceived = recv(s, buf, 4096, 0);
		if (bytesReceived <= 0) break;
		if (bytesReceived > 0)
		{
			// Echo response to console
			cout << string(buf, 0, bytesReceived) << endl;
			if (start == false)
			{
				start = true;
			}
			else 
			{
				cout << "> ";
			}
			
		}
	} while (true);

	return 0;
}

void main()
{
	// Terminal looks
	HANDLE hStdout = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(hStdout, FOREGROUND_GREEN | FOREGROUND_INTENSITY);
	SetConsoleTitle(".:: TCP Client ::. ");

	string ipAddress = "127.0.0.2";			// IP Address of the server
	int port = 54000;						// Listening port # on the server
	HANDLE hThread;
	DWORD dwThreadID;
	//------------------- //
	// Initialize Winsock //
	//------------------- //
	// Create Winsock object
	WSADATA wsData;
	// Request version 2.2
	WORD ver = MAKEWORD(2, 2);
	// Start up Winsock
	int wsOK = WSAStartup(ver, &wsData);
	if (wsOK != 0)
	{
		CleanSockets("Can't Initialize Winsock!", wsOK);
		return;
	}
	// --------------- //
	// Creat a socket: //
	// --------------- //
	//		- Adress familly AF_INET	--> IPv4 adresses
	//		- TCP server				--> SOCK_STREAM				(other option = DGRAM)
	//		- no protocol				--> 0
	sock = socket(AF_INET, SOCK_STREAM, 0);
	if (sock == INVALID_SOCKET)
	{
		CleanSockets("Can't Create a socket! Invalid socket.", WSAGetLastError());
		return;
	}
	else if (sock == SOCKET_ERROR)
	{
		CleanSockets("Can't Create a socket! Socket error.", WSAGetLastError());
		return;
	}
	//----------------------------------------- //
	// Bind an IP-adress and Port to the socket //
	//----------------------------------------- //
	// in --> ipv4 version of the structure
	sockaddr_in hint;
	hint.sin_family = AF_INET;
	// Networking is big endien, to move in between endiens, use htons = host to network shots. This makes it portable
	// Use port 54000
	hint.sin_port = htons(port);
	// Bind IP adress
	//		Adress family	-->	AF_INET
	//		Adress string	--> Your prefered IP_Adress
	//		buffer adress	--> ponter of hint adress
	inet_pton(AF_INET, ipAddress.c_str(), &hint.sin_addr);
	// ----------------- //
	// Connect to server //
	// ----------------- //
	int connResult = connect(sock, (sockaddr*)&hint, sizeof(hint));
	if (connResult != 0)
	{
		CleanSockets("Can't connect to server! Socket error.", WSAGetLastError());
		return;
	}
	// --------------------- //
	// Send and recieve data //
	// --------------------- //
	string userInput;
	// Send an initial buffer
	const char *sendbuf = "I joined";
	int iResult = send(sock, sendbuf, (int)strlen(sendbuf), 0);
	if (iResult == SOCKET_ERROR) {
		CleanSockets("send failed.", WSAGetLastError());
		return;
	}
	// Recieve Message
	hThread = CreateThread(NULL, 0, &ReadingThread, (void*)sock, 0, &dwThreadID);
	if (!hThread)
	{
		std::cout << "Error at CreateThread(): " << GetLastError() << std::endl;
		closesocket(sock);
		WSACleanup();
		return;
	}
	Sleep(1);
	do
	{
		// Prompt the user for some text
		cout << "\n> ";
		getline(cin, userInput); // you can have spaces insetad of multiple messages ('hello' 'world' --> 'hello world')
		if (userInput.size() > 0)		// Make sure the user has typed in something
		{
			// Send the text
			int sendResult = send(sock, userInput.c_str(), userInput.size() + 1, 0); //  + 1 because zero terminator
			if (sendResult == SOCKET_ERROR)
			{
				cout << "Server> Message failed to send. Err #" << WSAGetLastError() << endl;
			}
		}
		if (userInput == "end session")
		{
			break;
		}
	} while (userInput.size() >= 0);

	// ---------------- //
	// Close the socket //
	// ---------------- //
	closesocket(sock);

	// ---------------- //
	// Clean up Winsock //
	// ---------------- //
	WSACleanup();
}