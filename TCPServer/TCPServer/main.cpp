#include <iostream>
#include <string>
// Winsock 2nd version --> Windows uses to create sockets
#include <WS2tcpip.h> 

// Meant for the linker, this is the same as adding the lib in the project properties at Linker->Input->Additional dependencies
#pragma comment(lib, "ws2_32.lib")

// Is not great as a global var
using namespace std;

void main()
 {	
	string ipAddress = "127.0.0.1";			// IP Address of the server
	int port = 54000;						// Listening port # on the server
	//------------------- //
	// Initialize Winsock //
	//------------------- //
	// Create Winsock object
	WSADATA wsData;
	// Request version 2.2
	WORD ver = MAKEWORD(2, 2);
	// Start up Winsock
	int wsOK = WSAStartup(ver, &wsData);
	if (wsOK != 0)
	{
		// C error message
		cerr << "Can't Initialize Winsock! Err #" << wsOK << endl;
		return;
	}
	// --------------- //
	// Creat a socket: //
	// --------------- //

	//		- Adress familly AF_INET --> IPv4 adresses
	//		- TCP server --> SOCK_STREAM				(other option = DGRAM)
	SOCKET listening = socket(AF_INET, SOCK_STREAM, 0);
	if (listening == INVALID_SOCKET)
	{
		// C error message
		cerr << "Can't Create a socket! Err #" << WSAGetLastError() << endl;
		// Clean winsock
		WSACleanup();
	}
	//----------------------------------------- //
	// Bind an IP-adress and Port to the socket //
	//----------------------------------------- //
	// This is done using a hint structure
	sockaddr_in hint;
	hint.sin_family = AF_INET;
	// Networking is big endien, to move in between endiens, use htons = host to network shots
	// Use port 54000
	hint.sin_port = htons(port);
	// Bind to any IP adress
	hint.sin_addr.S_un.S_addr = INADDR_ANY; // You could also use inet_pton
	// Bind Socket to address
	bind(listening, (sockaddr*)&hint, sizeof(hint));

	//----------------------------------------- //
	// Tell Winsock the socket is for listening //
	//----------------------------------------- //
	// SOMAXCONN is the number of maximum connections we could have as a backlog --> stack up
	listen(listening, SOMAXCONN);

	// ------------------- //
	// Wait for connection //
	// ------------------- //
	sockaddr_in client;
	int clientSize = sizeof(client);

	SOCKET clientSocket = accept(listening, (sockaddr*)&client, &clientSize);
	if (clientSocket == INVALID_SOCKET)
	{
		cerr << "Can't Accept the listening socket! Err #" << WSAGetLastError() << endl;
		closesocket(listening);
		WSACleanup();
		return;
	}

	char host[NI_MAXHOST];		// Client's remote name
	char service[NI_MAXSERV];	// Service (i.e. port) the client is connected on

	ZeroMemory(host, NI_MAXHOST); // same as memset(host, 0, NI_MAXHOST);
	ZeroMemory(service, NI_MAXSERV);
	// If we get the name
	if (getnameinfo((sockaddr*)&client, sizeof(client), host, NI_MAXHOST, service, NI_MAXSERV, 0) == 0)
	{
		cout << host << "Connected on port " << service << endl;
	}
	else
	{
		inet_ntop(AF_INET, &client.sin_addr, host, NI_MAXHOST);
		cout << host << "Connected on port " << ntohs(client.sin_port) << endl;
	}

	// ---------------------- //
	// Close listening socket //
	// ---------------------- //
	closesocket(listening);

	// -------------------------------------- //
	// While loop: Accept and process message //
	// -------------------------------------- //
	// You might need a bigger buffer with bigger messages!
	char buf[4096];

	while (true)
	{
		ZeroMemory(buf, 4096);
		// Wait for client to send data
		int bytesReceived = recv(clientSocket, buf, 4096, 0);
		if (bytesReceived == SOCKET_ERROR)
		{
			cerr << "Error in recv(). Quitting" << endl;
			break;
		}
		if (bytesReceived == 0)
		{
			cout << "Client disconnected" << endl;
			break;
		}
		cout << "CLIENT:" << clientSocket << ">"<< string(buf, 0, bytesReceived) << endl;
		// Process message
		//		destination --> client socket
		//		message		--> buff
		//		length		--> bytes recieved + 1 (zerobuffer)
		send(clientSocket, buf, bytesReceived + 1, 0);
	}

	// ---------------- //
	// Close the socket //
	// ---------------- //
	closesocket(clientSocket);

	// ---------------- //
	// Clean up Winsock //
	// ---------------- //
	WSACleanup();
}

