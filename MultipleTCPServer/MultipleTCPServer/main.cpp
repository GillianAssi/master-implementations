#include <iostream>
#include <string>
// Winsock 2nd version --> Windows uses to create sockets
#include <WS2tcpip.h> 
#include <sstream>

// Meant for the linker, this is the same as adding the lib in the project properties at Linker->Input->Additional dependencies
#pragma comment(lib, "ws2_32.lib")

// Is not great as a global var
using namespace std;

void main()
{
	// Terminal looks
	HANDLE hStdout = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(hStdout, FOREGROUND_GREEN | FOREGROUND_INTENSITY);
	SetConsoleTitle(".:: TCP Server ::. ");

	string ipAddress = "127.0.0.1";			// IP Address of the server
	int port = 54000;						// Listening port # on the server
	//------------------- //
	// Initialize Winsock //
	//------------------- //
	// Create Winsock object
	WSADATA wsData;
	// Request version 2.2
	WORD ver = MAKEWORD(2, 2);
	// Start up Winsock
	int wsOK = WSAStartup(ver, &wsData);
	if (wsOK != 0)
	{
		// C error message
		cerr << "Can't Initialize Winsock! Err #" << wsOK << endl;
		return;
	}
	// --------------- //
	// Creat a socket: //
	// --------------- //

	//		- Adress familly AF_INET --> IPv4 adresses
	//		- TCP server --> SOCK_STREAM				(other option = DGRAM)
	SOCKET listening = socket(AF_INET, SOCK_STREAM, 0);
	if (listening == INVALID_SOCKET)
	{
		// C error message
		cerr << "Can't Create a socket! Err #" << WSAGetLastError() << endl;
		// Clean winsock
		WSACleanup();
	}
	//----------------------------------------- //
	// Bind an IP-adress and Port to the socket //
	//----------------------------------------- //
	// This is done using a hint structure
	sockaddr_in hint;
	hint.sin_family = AF_INET;
	// Networking is big endien, to move in between endiens, use htons = host to network shots
	// Use port 54000
	hint.sin_port = htons(port);
	// Bind to any IP adress
	hint.sin_addr.S_un.S_addr = INADDR_ANY; // You could also use inet_pton
	// Bind Socket to address
	bind(listening, (sockaddr*)&hint, sizeof(hint));

	//----------------------------------------- //
	// Tell Winsock the socket is for listening //
	//----------------------------------------- //
	// SOMAXCONN is the number of maximum connections we could have as a backlog --> stack up
	listen(listening, SOMAXCONN);

	//------------------ //
	// Force synchronous //
	//------------------ //
	// Define fd set
	fd_set master;
	// clear the set
	FD_ZERO(&master);
	// specify file descripter
	FD_SET(listening, &master);
	// Create running surver that handles multiple connections
	while (true)
	{
		// create a copy of the set --> every time we call select, it will be destroyed
		fd_set copy = master;
		// Synchronous IO queue
		int socketCount = select(0, &copy, nullptr, nullptr, nullptr); // sockets will go inside of copy

		for (int i = 0; i < socketCount; i++)
		{
			SOCKET sock = copy.fd_array[i];
			if (sock == listening)
			{
				// Accept a new connection
				//		socket				--> listening
				//		info about client	--> Null atm
				SOCKET client = accept(listening, nullptr, nullptr);
				// Add the new connection to the master file set (list of connected clients)
				FD_SET(client, &master);
				// Send a welcome message to the new client
				string welcomeMsg = "Welcome to this awesome Server!\r\n";
				send(client, welcomeMsg.c_str(), welcomeMsg.size() + 1, 0);
				cout << "SOCKET #" << client << " joined!" << endl;
				// TODO: Broadcast we have a new client
			}
			else
			{
				// Accept a new message
				// create a buffer
				char buf[4096];
				ZeroMemory(buf, 4096);
				int bytesIn = recv(sock, buf, 4096, 0);
				if (bytesIn <= 0)
				{
					// Drop client
					closesocket(sock);
					FD_CLR(sock, &master);
					cout << "SOCKET #" << sock << " left!" << endl;
					for (int i = 0; i < master.fd_count; i++)
					{
						SOCKET outSock = master.fd_array[i];
						if (outSock != listening && outSock != sock)
						{
							ostringstream ss;
							ss << "SOCKET #" << sock << " left!" << endl;
							string strOut = ss.str();
							send(outSock, strOut.c_str(), strOut.size() + 1, 0);
						}
					}

				}
				else
				{
					// NOTE IF YOU JOIN AT THE SAME TIME, THEY WON'T KNOW THIS
					// Send message to other clients, NOT the listening socket
					for (int i = 0; i < master.fd_count; i++)
					{
						SOCKET outSock = master.fd_array[i];
						if (outSock != listening && outSock != sock)
						{
							ostringstream ss;
							ss << "SOCKET #" << sock << ": " << buf << "\r\n";
							string strOut = ss.str();
							send(outSock, strOut.c_str(), strOut.size() + 1, 0);
						}
					}
				}
			}
		}
	}

	// ---------------- //
	// Clean up Winsock //
	// ---------------- //
	WSACleanup();
}
