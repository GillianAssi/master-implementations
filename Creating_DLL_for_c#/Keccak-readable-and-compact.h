#ifndef KECCAK_READABLE_AND_COMPACT_H
#define KECCAK_READABLE_AND_COMPACT_H

#pragma once


#define __STDC_WANT_LIB_EXT1__ 1
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#define MIN(a, b) ((a) < (b) ? (a) : (b))
// effect the linkage
#ifdef __cplusplus
	extern "C" {
#endif

// Build option
#ifdef BUILD_MY_DLL
	#define KECCAK_READABLE_AND_COMPACT __declspec(dllexport)
#else
	#define KECCAK_READABLE_AND_COMPACT __declspec(dllimport)
#endif


void Keccak(unsigned int rate, unsigned int capacity, const unsigned char *input, unsigned long long int inputByteLen, unsigned char delimitedSuffix, unsigned char *output, unsigned long long int outputByteLen);
void FIPS202_SHAKE128(const unsigned char *input, unsigned int inputByteLen, unsigned char *output, int outputByteLen);
void FIPS202_SHAKE256(const unsigned char *input, unsigned int inputByteLen, unsigned char *output, int outputByteLen);
void FIPS202_SHA3_224(const unsigned char *input, unsigned int inputByteLen, unsigned char *output);
void FIPS202_SHA3_256(const unsigned char *input, unsigned int inputByteLen, unsigned char *output);
void FIPS202_SHA3_384(const unsigned char *input, unsigned int inputByteLen, unsigned char *output);
void FIPS202_SHA3_512(const unsigned char *input, unsigned int inputByteLen, unsigned char *output);

#ifdef __cplusplus
	}
#endif

#endif // end of DLL
