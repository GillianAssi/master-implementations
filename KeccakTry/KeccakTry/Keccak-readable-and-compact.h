#pragma once

#define KECCAK_READABLE_AND_COMPACT
#define __STDC_WANT_LIB_EXT1__ 1
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#define MIN(a, b) ((a) < (b) ? (a) : (b))
class Keccak_readable_and_compact
{
public:
	void Keccak(unsigned int rate, unsigned int capacity, const unsigned char *input, unsigned long long int inputByteLen, unsigned char delimitedSuffix, unsigned char *output, unsigned long long int outputByteLen);
	void FIPS202_SHAKE128(const unsigned char *input, unsigned int inputByteLen, unsigned char *output, int outputByteLen);
	void FIPS202_SHAKE256(const unsigned char *input, unsigned int inputByteLen, unsigned char *output, int outputByteLen);
	void FIPS202_SHA3_224(const unsigned char *input, unsigned int inputByteLen, unsigned char *output);
	void FIPS202_SHA3_256(const unsigned char *input, unsigned int inputByteLen, unsigned char *output);
	void FIPS202_SHA3_384(const unsigned char *input, unsigned int inputByteLen, unsigned char *output);
	void FIPS202_SHA3_512(const unsigned char *input, unsigned int inputByteLen, unsigned char *output);
};

