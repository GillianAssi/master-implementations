// KeccakTry.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
using namespace std; // std = standard

int main()
{	
	Keccak_readable_and_compact f;
	const char  message[] = "blabla";
	const unsigned char* input = reinterpret_cast<const unsigned char *>(message);
	unsigned int inputByteLen = sizeof(message)-1;
	const int outputByteLen = 5;
	unsigned char output[outputByteLen];

	// Use shake
	f.FIPS202_SHAKE128(input, inputByteLen, output, outputByteLen);

	// Print
	for (int i = 0; i < inputByteLen; ++i)
		printf("%c", input[i]);
	printf("\n");
	for (int i = 0; i < outputByteLen; ++i)
		printf("%.2x", output[i]);
	printf("\n");

	system("pause");
	return 0;
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
