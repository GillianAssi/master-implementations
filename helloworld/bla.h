#ifndef KECCAK_READABLE_AND_COMPACT
    #define KECCAK_READABLE_AND_COMPACT
    #define __STDC_WANT_LIB_EXT1__ 1
    #include <stdint.h>
    #include <stdlib.h>
    #include <string.h>
    #define MIN(a, b) ((a) < (b) ? (a) : (b))

    void Keccak(unsigned int rate, unsigned int capacity, const unsigned char *input, unsigned long long int inputByteLen, unsigned char delimitedSuffix, unsigned char *output, unsigned long long int outputByteLen);

#endif
