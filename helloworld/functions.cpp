#include "functions.h"

// give a 32 char array
void functions::getUid(char* uid) {
	int r;
	for (int i = 0; i < 32; i++) {
		r = rand() % 26;
		uid[i] = 'a' + r;
	}
	uid[32] = { '\0' };
}