﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices; // nodig voor c++ dll

namespace Keccak_Try_fromCSharp
{
    class Program
    {
        //Applied to a return value.
        [return: MarshalAs(UnmanagedType.LPStr)]
        public String GetMessage()
        {
            return "blabla";
        }

        [DllImport("D:\\Unief\\1MaIndi\\Master Proef\\projects\\Creating_DLL_for_c#\\Keccak_readable_and_compact.dll", CallingConvention = CallingConvention.Cdecl)]
        static extern void Keccak(uint rate, uint capacity, byte[] input, ulong inputByteLen, char delimitedSuffix, byte[] output, ulong outputByteLen);
        [DllImport("D:\\Unief\\1MaIndi\\Master Proef\\projects\\Creating_DLL_for_c#\\Keccak_readable_and_compact.dll", CallingConvention = CallingConvention.Cdecl)]
        static extern void FIPS202_SHAKE128(byte[] input, uint inputByteLen, byte[] output, uint outputByteLen);
        [DllImport("D:\\Unief\\1MaIndi\\Master Proef\\projects\\Creating_DLL_for_c#\\Keccak_readable_and_compact.dll", CallingConvention = CallingConvention.Cdecl)]
        static extern void FIPS202_SHAKE256(byte[] input, uint inputByteLen, byte[] output, uint outputByteLen);
        [DllImport("D:\\Unief\\1MaIndi\\Master Proef\\projects\\Creating_DLL_for_c#\\Keccak_readable_and_compact.dll", CallingConvention = CallingConvention.Cdecl)]
        static extern void FIPS202_SHA3_224(byte[] input, uint inputByteLen, byte[] output);
        [DllImport("D:\\Unief\\1MaIndi\\Master Proef\\projects\\Creating_DLL_for_c#\\Keccak_readable_and_compact.dll", CallingConvention = CallingConvention.Cdecl)]
        static extern void FIPS202_SHA3_256(byte[] input, uint inputByteLen, byte[] output);
        [DllImport("D:\\Unief\\1MaIndi\\Master Proef\\projects\\Creating_DLL_for_c#\\Keccak_readable_and_compact.dll", CallingConvention = CallingConvention.Cdecl)]
        static extern void FIPS202_SHA3_384(byte[] input, uint inputByteLen, byte[] output);
        [DllImport("D:\\Unief\\1MaIndi\\Master Proef\\projects\\Creating_DLL_for_c#\\Keccak_readable_and_compact.dll", CallingConvention = CallingConvention.Cdecl)]
        static extern void FIPS202_SHA3_512(byte[] input, uint inputByteLen, byte[] output);

        public static string ByteArrayToString(byte[] ba)
        {
            StringBuilder hex = new StringBuilder(ba.Length * 2);
            foreach (byte b in ba)
                hex.AppendFormat("{0:x2}", b);
            return hex.ToString();
        }

        static void Main(string[] args)
        {
            Program p = new Program();
            string message = "blabla";
            byte[] input = Encoding.UTF8.GetBytes(message);
	        uint inputByteLen = Convert.ToUInt32(message.Length);
	        const uint outputByteLen = 5;
            byte[] output = new byte[outputByteLen];
            FIPS202_SHAKE128(input, inputByteLen,output, outputByteLen);
            Console.WriteLine(ByteArrayToString(input));
            Console.WriteLine(ByteArrayToString(output));
            Console.ReadKey();
        }

    }
}
