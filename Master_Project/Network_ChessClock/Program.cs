﻿using System;
using System.Timers;
using System.Runtime.CompilerServices;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices; // nodig voor c++ dll
using NaCl; // for salsa


namespace Network_ChessClock
{
    class Program
    {
        //--------------- initiate timer for initResponse resend
        private static System.Timers.Timer timer;
        private static NetworkStream stream;
        private static Stopwatch stopwatch;
        // keys
        private static byte[] firstkey = new byte[]
        {
                0x1b, 0x27, 0x55, 0x64, 0x73, 0xe9, 0x85,
                0xd4, 0x62, 0xcd, 0x51, 0x19, 0x7a, 0x9a,
                0x46, 0xc7, 0x60, 0x09, 0x54, 0x9e, 0xac,
                0x64, 0x74, 0xf2, 0x06, 0xc4, 0xee, 0x08,
                0x44, 0xf6, 0x83, 0x89
        };
        private static byte[] key = firstkey;
        private static byte[] prevKey = key;
        private static byte[] NewKeyBase;
        private static byte[] nonce = new byte[24]; 
        // message
        private static byte[] ack = { 0x01 };
        private static byte[] init = { 0x02 };
        private static byte[] lastMessage = new byte[64];
        // socket
        private static int port = 8000;
        private static IPAddress ip = Dns.GetHostEntry("localhost").AddressList[0];
        // bools
        private static bool initiated = false;
        private static bool completeInitiation = false;
        // Import functions
        [DllImport("D:\\Unief\\1MaIndi\\Master Proef\\projects\\Creating_DLL_for_c#\\Keccak_readable_and_compact.dll", CallingConvention = CallingConvention.Cdecl)]
        static extern void Keccak(uint rate, uint capacity, byte[] input, ulong inputByteLen, char delimitedSuffix, byte[] output, ulong outputByteLen);
        [DllImport("D:\\Unief\\1MaIndi\\Master Proef\\projects\\Creating_DLL_for_c#\\Keccak_readable_and_compact.dll", CallingConvention = CallingConvention.Cdecl)]
        static extern void FIPS202_SHAKE128(byte[] input, uint inputByteLen, byte[] output, uint outputByteLen);
        [DllImport("D:\\Unief\\1MaIndi\\Master Proef\\projects\\Creating_DLL_for_c#\\Keccak_readable_and_compact.dll", CallingConvention = CallingConvention.Cdecl)]
        static extern void FIPS202_SHAKE256(byte[] input, uint inputByteLen, byte[] output, uint outputByteLen);
        [DllImport("D:\\Unief\\1MaIndi\\Master Proef\\projects\\Creating_DLL_for_c#\\Keccak_readable_and_compact.dll", CallingConvention = CallingConvention.Cdecl)]
        static extern void FIPS202_SHA3_224(byte[] input, uint inputByteLen, byte[] output);
        [DllImport("D:\\Unief\\1MaIndi\\Master Proef\\projects\\Creating_DLL_for_c#\\Keccak_readable_and_compact.dll", CallingConvention = CallingConvention.Cdecl)]
        static extern void FIPS202_SHA3_256(byte[] input, uint inputByteLen, byte[] output);
        [DllImport("D:\\Unief\\1MaIndi\\Master Proef\\projects\\Creating_DLL_for_c#\\Keccak_readable_and_compact.dll", CallingConvention = CallingConvention.Cdecl)]
        static extern void FIPS202_SHA3_384(byte[] input, uint inputByteLen, byte[] output);
        [DllImport("D:\\Unief\\1MaIndi\\Master Proef\\projects\\Creating_DLL_for_c#\\Keccak_readable_and_compact.dll", CallingConvention = CallingConvention.Cdecl)]
        static extern void FIPS202_SHA3_512(byte[] input, uint inputByteLen, byte[] output);

        /// <summary>
        /// 
        /// </summary>
        private static void SetTimer()
        {
            // Create a timer with a two second interval.
            timer = new System.Timers.Timer(5000);
            // Hook up the Elapsed event for the timer. 
            timer.Elapsed += OnTimedEvent;
            timer.AutoReset = true;
            timer.Enabled = true;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        private static void OnTimedEvent(Object source, ElapsedEventArgs e)
        {
            Console.WriteLine("Resend of InitResponse initiated");
            try
            {
                // send
                stream.Write(lastMessage, 0, lastMessage.Length);
                Console.WriteLine("InitResponse sent");
            }
            catch
            {
                Console.WriteLine("failed to send InitResponse message");
            }
            SetTimer();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ba"></param>
        /// <returns></returns>
        public static string ByteArrayToString(byte[] ba)
        {
            StringBuilder hex = new StringBuilder(ba.Length * 2);
            foreach (byte b in ba)
                hex.AppendFormat("{0:x2}", b);
            return hex.ToString();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="source"></param>
        /// <param name="separator"></param>
        /// <returns></returns>
        public static byte[] SeparateAndGetLast(byte[] source, byte[] separator)
        {
            for (var i = 0; i < source.Length; ++i)
            {
                if (Equals(source, separator, i))
                {

                    var index = i + separator.Length;
                    var part = new byte[source.Length - index];
                    Array.Copy(source, index, part, 0, part.Length);
                    return part;
                }
            }
            throw new Exception("not found");
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="source"></param>
        /// <param name="separator"></param>
        /// <returns></returns>
        public static byte[][] Separate(byte[] source, byte[] separator)
        {
            var Parts = new List<byte[]>();
            var Index = 0;
            byte[] Part;
            for (var i = 0; i < source.Length; ++i)
            {
                if (Equals(source, separator, i))
                {

                    Part = new byte[i - Index];
                    Array.Copy(source, Index, Part, 0, Part.Length);
                    Parts.Add(Part);
                    Index = i + separator.Length;
                    i += separator.Length - 1;
                }
            }
            Part = new byte[source.Length - Index];
            Array.Copy(source, Index, Part, 0, Part.Length);
            Parts.Add(Part);
            return Parts.ToArray();
        }
        public static bool Equals(byte[] source, byte[] separator, int index)
        {
            for (int i = 0; i < separator.Length; ++i)
                if (index + i >= source.Length || source[index + i] != separator[i])
                    return false;
            return true;
        }
        // remove tailing zeroes
        /// <summary>
        /// 
        /// </summary>
        /// <param name="packet"></param>
        /// <returns></returns>
        public static byte[] Compact(byte[] packet)
        {
            var i = packet.Length - 1;
            while (packet[i] == 0)
            {
                --i;
            }
            var temp = new byte[i + 1];
            Array.Copy(packet, temp, i + 1);
            return temp;
        }
        /// <summary>
        /// Authenticate a message
        /// nonce|length|Servermessage|Auth
        /// This function authenticates a message by comparing the received SHA3 has with
        /// a self-hashed version of the received message
        /// <summary>
        /// <param name="receivedBuffer">Complete buffer received.</param>
        /// <returns>The encrypted ServerMessage if the authentication is successfull</returns>
        public bool AuthenticateMessage(byte[] receivedBuffer,out byte[] encryptedServerMessage, byte[] currentkey)
        {
            // remove tailing zeros to only obtain the CompleteMessage
            byte[] completeMessage = Compact(receivedBuffer);
            Console.WriteLine("You received:'{0}'", ByteArrayToString(completeMessage));
            // seperate Nonce, ServerMessage and Authentication
            // assigne nonce and serverMessage
            // assigne nonce and serverMessage
            Array.Copy(completeMessage, 0, nonce, 0, 24);
            byte[] convertLength = new byte[4];
            byte[] length = new byte[2];
            Array.Copy(completeMessage, 24, convertLength, 0, 2);
            Array.Copy(completeMessage, 24, length, 0, 2);
            encryptedServerMessage = new byte[BitConverter.ToInt32(convertLength, 0)];
            Array.Copy(completeMessage, 26, encryptedServerMessage, 0, BitConverter.ToInt32(convertLength, 0));
            byte[] auth = new byte[32];
            Array.Copy(completeMessage, 26 + BitConverter.ToInt32(convertLength, 0), auth, 0, 32);
            // Hash the given key with full Servermessag
            byte[][] combine4 = new byte[4][];
            combine4[0] = currentkey; // key 32 byte
            combine4[1] = nonce;  // nonce 24 byte
            combine4[2] = length; // length 4 byte
            combine4[3] = encryptedServerMessage; // servermessage length byte
            byte[] input = CombineVariablesBytes(combine4);
            uint inputByteLen = Convert.ToUInt32(input.Length);
            const uint outputByteLen = 32;
            byte[] output = new byte[outputByteLen];
            FIPS202_SHAKE128(input, inputByteLen, output, outputByteLen);
            if (auth.SequenceEqual(output)) // compare with auth
            {
                byte[][] combine3 = new byte[3][];
                combine3[0] = nonce;  // nonce
                combine3[1] = length; // length
                combine3[2] = encryptedServerMessage; // servermessage
                NewKeyBase = CombineVariablesBytes(combine3);
                return true;
            }
            else
            {
                Console.WriteLine("ERROR: Authentication failed");
                return false;
            }
        }
        /// <summary>
        /// Handle Message
        /// type|msg|nonce
        /// This function will break down a received server message and update the nessecary parameters.
        /// </summary>
        /// <param name="encryptedServerMessage">Message sent by the server containing an ActionMessage and ServerNonce</param>
        /// <returns>The action message</returns>
        public byte[] HandleMessage(byte[] encryptedServerMessage, byte[] key)
        {
            // Decrypt ServerMessage
            byte[] serverMessage = new byte[encryptedServerMessage.Length];
            XSalsa20 xSalsa20 = new XSalsa20(key);
            xSalsa20.Transform(serverMessage, encryptedServerMessage, nonce); // nonce already saved in auth
            Console.WriteLine("You decrypted:'{0}'", Encoding.UTF8.GetString(serverMessage, 0, serverMessage.Length));
            // seperate type,ActionMessage and ServerNonce
            byte[] type = new byte[1];
            Array.Copy(serverMessage, 0, type, 0, 1);
            string msg = "";
            if (type.SequenceEqual(ack)) // 0x01
            // the message has the form ack|ServerMessage
            {
                byte[] ackMessage = new byte[serverMessage.Length - 1];
                Array.Copy(serverMessage, 1, ackMessage, 0, serverMessage.Length - 1);
                msg = Encoding.UTF8.GetString(ackMessage, 0, ackMessage.Length);
                Console.WriteLine("Ack reveived with message:'{0}'", msg);
                GenerateNewKey(NewKeyBase); // start normal absorb
                return ackMessage;
            }
            else if (type.SequenceEqual(init)) // 0x02
                                               // the message has the form init|R1
            {
                byte[] r1 = new byte[32];
                Array.Copy(serverMessage, 1, r1, 0, 32);
                try
                {
                    Console.WriteLine("Initiation message received");
                    //GenerateNewKey(NewKeyBase);
                    initiated = SendInitResponse(r1, nonce);
                }

                catch (Exception ex)
                {
                    Console.WriteLine("Something went wrong while sending the initiation response");
                    Console.WriteLine("Error: " + ex.ToString());
                    return null;
                }
                return null;
            }
            else // actionMessage
            // the message has the form msgbyte|ServerMessage
            {
                byte[] actionMessage = new byte[serverMessage.Length - 1];
                Array.Copy(serverMessage, 1, actionMessage, 0, serverMessage.Length - 1);
                msg = Encoding.UTF8.GetString(actionMessage, 0, actionMessage.Length);
                Console.WriteLine("ActionMessage reveived with message:'{0}'", msg);
                if (completeInitiation)
                {
                    GenerateNewKey(NewKeyBase);
                    SendAck("Message succesfully received");
                    GenerateNewKey(NewKeyBase);
                    return actionMessage;
                }
                else
                {
                    Console.WriteLine("Initiation not yet completed, discard message");
                    return null;
                }
                
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="statusMessage"></param>
        /// <param name="ts"></param>
        /// <param name="stream"></param>
        /// <returns></returns>
        public bool SendAck(string statusMessage)
        {
            // convert statusMessage to bytes
            byte[] byteStatusMessage = new byte[Encoding.UTF8.GetByteCount(statusMessage)] ;
            byteStatusMessage = Encoding.UTF8.GetBytes(statusMessage);
            // create the new ClockNonce
            TimeSpan ts = stopwatch.Elapsed;
            string clockNonce = (ts.Seconds).ToString();
            byte[] byteClockNonce = new byte[24];
            System.Buffer.BlockCopy(Encoding.UTF8.GetBytes(clockNonce), 0, byteClockNonce, 0, Encoding.UTF8.GetBytes(clockNonce).Length);
            // Combine Ack byte + message length + statusMessage to create the rawClockMessage
            byte[][] combine2 = new byte[2][];
            combine2[0] = ack;  // 1 byte
            combine2[1] = byteStatusMessage; // 32bytes
            byte[] rawClockMessage = CombineVariablesBytes(combine2);
            // Encrypt to get the ClockMessage
            byte[] clockMessage = new byte[rawClockMessage.Length];
            XSalsa20 xSalsa20 = new XSalsa20(key);
            xSalsa20.Transform(clockMessage, rawClockMessage, byteClockNonce);
            // set length
            byte[] messageLength = new byte[2];
            System.Buffer.BlockCopy(BitConverter.GetBytes(clockMessage.Length), 0, messageLength, 0, 2);
            // Add nonce + messageLength + ClockMessage
            byte[][] combine3 = new byte[3][];
            combine3[0] = byteClockNonce;
            combine3[1] = messageLength;
            combine3[2] = clockMessage;
            byte[] fullClockMessage = CombineVariablesBytes(combine3);
            NewKeyBase = fullClockMessage;
            // Add the key to create authentication
            combine2 = new byte[2][];
            combine2[0] = key;
            combine2[1] = fullClockMessage;
            byte[] rawAuth = CombineVariablesBytes(combine2);
            // hash
            uint inputByteLen = Convert.ToUInt32(rawAuth.Length);
            const uint outputByteLen = 32;
            byte[] auth = new byte[outputByteLen];
            FIPS202_SHAKE128(rawAuth, inputByteLen, auth, outputByteLen);
            // combine ClockMessage with Authentication
            combine2 = new byte[2][];
            combine2[0] = fullClockMessage;
            combine2[1] = auth;
            byte[] completeClockMessage = CombineVariablesBytes(combine2);
            try
            {
                // send
                stream.Write(completeClockMessage, 0, completeClockMessage.Length);
                Console.WriteLine("AckMessage sent");
                return true;
            }
            catch
            {
                Console.WriteLine("failed to send AckMessage");
                return false;
            }
        }
        public bool GenerateNewKey(byte[] input)
        {
            try
            {
                // Generate new key using the last key and the RawClockMessage
                byte[][] combine2 = new byte[2][];
                combine2[0] = key;
                combine2[1] = input;
                byte[] rawKey = CombineVariablesBytes(combine2);
                uint inputByteLen = Convert.ToUInt32(input.Length);
                const uint outputByteLen2 = 32;
                byte[] output = new byte[outputByteLen2];
                FIPS202_SHAKE128(input, inputByteLen, output, outputByteLen2);
                // Update previous variables
                prevKey = key;
                key = output;
                Console.WriteLine("New Key:'{0}'", Encoding.UTF8.GetString(key, 0, key.Length));
                return true;
            }
            catch
            {
                return false;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="m1"></param>
        /// <param name="t1"></param>
        /// <param name="stopWatch"></param>
        /// <param name="stream"></param>
        /// <returns></returns>
        public bool SendInitResponse(byte[] r1, byte[] t1)
        {
            // generate T2 --> 32 byte
            TimeSpan ts = stopwatch.Elapsed;
            string clockNonce = ts.Seconds.ToString();
            byte[] t2 = new byte[24];
            System.Buffer.BlockCopy(Encoding.UTF8.GetBytes(clockNonce), 0, t2, 0, Encoding.UTF8.GetBytes(clockNonce).Length);
            // generate R2 --> 32 byte
            Random rnd = new Random();
            Byte[] r2 = new Byte[32];
            rnd.NextBytes(r2);
            Console.WriteLine("R1  = " + Encoding.UTF8.GetString(r1) + "' and T1 = '" + Encoding.UTF8.GetString(t1) + "'");
            Console.WriteLine("R2  = " + Encoding.UTF8.GetString(r2) + "' and T1 = '" + Encoding.UTF8.GetString(t2) + "'");
            // Generate sharedKey from all R's and T's
            byte[][] combine4 = new byte[4][];
            combine4[0] = r1;
            combine4[1] = r2;
            combine4[2] = t1;
            combine4[3] = t2;
            byte[] rawSharedKey = CombineVariablesBytes(combine4);
            uint inputByteLen = Convert.ToUInt32(rawSharedKey.Length);
            const uint outputByteLen = 32;
            byte[] sharedKey = new byte[outputByteLen];
            FIPS202_SHAKE128(rawSharedKey, inputByteLen, sharedKey, outputByteLen);
            // Generate M3 -------------- NOG STEEDS FIRST KEY
            byte[][] combine2 = new byte[2][];
            combine2[0] = sharedKey;
            combine2[1] = firstkey;
            byte[] rawM3 = CombineVariablesBytes(combine2);
            inputByteLen = Convert.ToUInt32(rawM3.Length);
            byte[] m3 = new byte[outputByteLen];
            FIPS202_SHAKE128(rawM3, inputByteLen, m3, outputByteLen);
            // generate new key with firstKey, r1 and r2
            byte[][] combine3 = new byte[3][];
            combine3[0] = firstkey;
            combine3[1] = r1;
            combine3[2] = r2;
            byte[] rawKey = CombineVariablesBytes(combine3);
            GenerateNewKey(rawKey);
            // generate full response
            combine3 = new byte[3][];
            combine3[0] = init;
            combine3[1] = r2;
            combine3[2] = m3;
            byte[] fullResponse = CombineVariablesBytes(combine3);
            // Generate complete response (encrypted full response)
            byte[] encryptedFullResponse = new byte[fullResponse.Length];
            XSalsa20 xSalsa20 = new XSalsa20(firstkey);
            xSalsa20.Transform(encryptedFullResponse, fullResponse, t2);
            // set length
            byte[] messageLength = new byte[2];
            System.Buffer.BlockCopy(BitConverter.GetBytes(encryptedFullResponse.Length), 0, messageLength, 0, 2);
            // generate complete response
            combine3 = new byte[3][];
            combine3[0] = t2; // 24 bytes
            combine3[1] = messageLength; // 2 bytes
            combine3[2] = encryptedFullResponse; // length bytes
            byte[] completeResponse = CombineVariablesBytes(combine3);
            // generate authentication
            combine2 = new byte[2][];
            combine2[0] = firstkey;
            combine2[1] = completeResponse;
            byte[] rawAuth = CombineVariablesBytes(combine2);
            inputByteLen = Convert.ToUInt32(rawAuth.Length);
            byte[] auth = new byte[outputByteLen];
            FIPS202_SHAKE128(rawAuth, inputByteLen, auth, outputByteLen);
            // generate init respons
            combine2 = new byte[2][];
            combine2[0] = completeResponse;
            combine2[1] = auth;
            byte[] initResponse = CombineVariablesBytes(combine2);
            lastMessage = initResponse;
            try
            {
                // send
                stream.Write(initResponse, 0, initResponse.Length);
                Console.WriteLine("InitResponse sent");
                return true;
            }
            catch
            {
                Console.WriteLine("failed to send InitResponse message");
                return false;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="variables"></param>
        /// <returns></returns>
        public byte[] CombineVariablesBytes(byte[][] variables)
        {
            int bytelength = 0;
            for (int i = 0; i< variables.Length; i++)
            {
                bytelength += variables[i].Length;
            }
            byte[] combinedResult = new byte[bytelength];
            bytelength = 0;
            for (int i = 0; i< variables.Length; i++)
            {
                System.Buffer.BlockCopy(variables[i], 0, combinedResult, bytelength, variables[i].Length);
                bytelength += variables[i].Length;
            }

            return combinedResult;
        }


        static void Main(string[] args)
        {
            Program p = new Program();
            //--------------- start timer for nonce
            stopwatch = new Stopwatch();
            stopwatch.Start();
           
            //--------------- Create a socket listener
            TcpClient client = default(TcpClient);
            TcpListener server = new TcpListener(ip, port);
            bool started = false;
            // Listent
            try
            {
                while(started == false)
                {
                    server = new TcpListener(ip, port); // current clock port
                    try
                    {
                        server.Start();
                        Console.WriteLine("Clock started on port " + port + " ...");
                        started = true;
                    }
                    catch
                    {
                        port++;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                // Don't close immediatelly
                Console.Read();
            }
            bool auth = false;
            //--------------- Start loop
            while (true)
            {
                byte[] actionmessage;
                //--------------- Connect to client
                Console.Write("\n>");
                // client is connected
                client = server.AcceptTcpClient();
                // bytes that are going to be received by the server
                byte[] receivedBuffer = new byte[256];
                // Stream for the data to come trough (of the client)
                stream = client.GetStream();   
                // read the network stream, in the receivedBuffer, starting on the 0'th byte
                stream.Read(receivedBuffer, 0, receivedBuffer.Length);
                // Authenticate message
                try
                {
                    byte[] servermessage = new byte[32];
                    auth = p.AuthenticateMessage(receivedBuffer, out servermessage, key);
                    if (auth)
                    {
                        if (completeInitiation == true) // Handle an ActionMessage
                        {
                            try
                            {
                                actionmessage = p.HandleMessage(servermessage, key);
                            }
                            catch (Exception ex)
                            {
                                Console.Write("ActionMessage handeling failed with exception: " + ex.ToString());
                            }
                        }
                        else if (initiated) // handle an ack
                        {
                            try
                            {
                                p.HandleMessage(servermessage, key);
                                completeInitiation = true; // initiation complete
                                //timer.Dispose();
                            }
                            catch (Exception ex)
                            {
                                Console.Write("Ackmessage handeling failed with exception: " + ex.ToString());
                            }
                        }
                        else // Handle an initiation message
                        {
                            p.HandleMessage(servermessage, key);
                            //SetTimer();
                        }
                    }

                    else if (initiated) 
                    {
                        auth = p.AuthenticateMessage(receivedBuffer, out servermessage, prevKey);
                        if (auth)
                        {
                            key = prevKey; // something went wrong, update the keys back 
                            if (completeInitiation) // Handle ActionMessage again.
                            {
                                Console.Write("previous key worked");
                                actionmessage = p.HandleMessage(servermessage, key);
                            }
                            else // our initation respons was not received, handle the initiation message
                            {
                                p.HandleMessage(servermessage, firstkey);
                            }
                        }
                        else
                        {
                            Console.Write("Authentication Failed: Message discarded.");
                        }
                    }
                    // no need to test second key if its not initiated
                    else { Console.Write("This isn't an initiation message: Message discarded."); }
                }
                catch (Exception ex)
                {
                    Console.Write("Error: "+ex.ToString());
                }
                
            }
        }
    }
}
