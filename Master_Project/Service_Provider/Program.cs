﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices; // nodig voor c++ dll
using NaCl; // for salsa

using FireSharp.Config;
using FireSharp.Interfaces;
using FireSharp.Response;

namespace Service_Provider
{
    class Program
    {
        [DllImport("D:\\Unief\\1MaIndi\\Master Proef\\projects\\Creating_DLL_for_c#\\Keccak_readable_and_compact.dll", CallingConvention = CallingConvention.Cdecl)]
        static extern void Keccak(uint rate, uint capacity, byte[] input, ulong inputByteLen, char delimitedSuffix, byte[] output, ulong outputByteLen);
        [DllImport("D:\\Unief\\1MaIndi\\Master Proef\\projects\\Creating_DLL_for_c#\\Keccak_readable_and_compact.dll", CallingConvention = CallingConvention.Cdecl)]
        static extern void FIPS202_SHAKE128(byte[] input, uint inputByteLen, byte[] output, uint outputByteLen);
        [DllImport("D:\\Unief\\1MaIndi\\Master Proef\\projects\\Creating_DLL_for_c#\\Keccak_readable_and_compact.dll", CallingConvention = CallingConvention.Cdecl)]
        static extern void FIPS202_SHAKE256(byte[] input, uint inputByteLen, byte[] output, uint outputByteLen);
        [DllImport("D:\\Unief\\1MaIndi\\Master Proef\\projects\\Creating_DLL_for_c#\\Keccak_readable_and_compact.dll", CallingConvention = CallingConvention.Cdecl)]
        static extern void FIPS202_SHA3_224(byte[] input, uint inputByteLen, byte[] output);
        [DllImport("D:\\Unief\\1MaIndi\\Master Proef\\projects\\Creating_DLL_for_c#\\Keccak_readable_and_compact.dll", CallingConvention = CallingConvention.Cdecl)]
        static extern void FIPS202_SHA3_256(byte[] input, uint inputByteLen, byte[] output);
        [DllImport("D:\\Unief\\1MaIndi\\Master Proef\\projects\\Creating_DLL_for_c#\\Keccak_readable_and_compact.dll", CallingConvention = CallingConvention.Cdecl)]
        static extern void FIPS202_SHA3_384(byte[] input, uint inputByteLen, byte[] output);
        [DllImport("D:\\Unief\\1MaIndi\\Master Proef\\projects\\Creating_DLL_for_c#\\Keccak_readable_and_compact.dll", CallingConvention = CallingConvention.Cdecl)]
        static extern void FIPS202_SHA3_512(byte[] input, uint inputByteLen, byte[] output);

        
        

        public static string ByteArrayToString(byte[] ba)
        {
            StringBuilder hex = new StringBuilder(ba.Length * 2);
            foreach (byte b in ba)
                hex.AppendFormat("{0:x2}", b);
            return hex.ToString();
        }
        // ontvang info van firebase
        private static async void RetrieveKey(IFirebaseClient firebaseClient, string id)
        {
            FirebaseResponse response = await firebaseClient.GetAsync("information/" + id);
            if (response.Body != "null")
            {
                Data obj = response.ResultAs<Data>();
                Console.WriteLine("\n Naam = " + obj.Name);
                Console.WriteLine("\n Age = " + obj.Age);
                Console.WriteLine("\n Adress = " + obj.Adress);
                Console.WriteLine("\n id = " + obj.Id);
            }
            else
            {
                Console.WriteLine("Incorrect ID");
            }

        }
        // Update info op firebase
        private static async void UpdateKeys(IFirebaseClient firebaseClient, string id, Data data)
        {
            FirebaseResponse response = await firebaseClient.PushAsync("information/" + id, data);
            //SetResponse response = await client.SetTaskAsync("information/"+textBox2.Text,data);
            Data result = response.ResultAs<Data>();

            Console.WriteLine("Data inserted of ID" + result.Id);
        }
        // Delete info op firebase
        private static async void DeleteClock(IFirebaseClient firebaseClient, string id)
        {
            FirebaseResponse response = await firebaseClient.DeleteAsync("information/" + id);
            Console.WriteLine("Clock deleted of ID " + id);
        }
        static void Main(string[] args)
        {
            // Variables
            string serverIP = "localhost";
            int port = 8080;
            string userInput;
            int byteCount;
            // firebase
            IFirebaseClient firebaseClient;
            IFirebaseConfig config = new FirebaseConfig
            {
                AuthSecret = "WMZC9oaHVt7T9lwtyutMrhDrREgyZ3mDDmZl0HDW",
                BasePath = "https://ctry-ee061.firebaseio.com/"
            };
            // keys
            var firstkey = new byte[]
            {
                0x1b, 0x27, 0x55, 0x64, 0x73, 0xe9, 0x85,
                0xd4, 0x62, 0xcd, 0x51, 0x19, 0x7a, 0x9a,
                0x46, 0xc7, 0x60, 0x09, 0x54, 0x9e, 0xac,
                0x64, 0x74, 0xf2, 0x06, 0xc4, 0xee, 0x08,
                0x44, 0xf6, 0x83, 0x89
            };
            var nonce = new byte[]
                {
                0x69, 0x69, 0x6e, 0xe9, 0x55, 0xb6,
                0x2b, 0x73, 0xcd, 0x62, 0xbd, 0xa8,
                0x75, 0xfc, 0x73, 0xd6, 0x82, 0x19,
                0xe0, 0x03, 0x6b, 0x7a, 0x0b, 0x37
                };

            // connect to firebase
            firebaseClient = new FireSharp.FirebaseClient(config);
            if (firebaseClient != null)
            {
                Console.WriteLine("\n Firebase connection Established");
            }
            // What do I want:
            // --------------- start TCP client --------------- //
            // --------------- Ask which ID --------------- //
            // --------------- Get key --------------- //
            // --------------- Encrypt using key --------------- //
            // --------------- Hash key using message --------------- //
            // --------------- Update firebase --------------- //

            do
            {
                // --------------- start TCP client --------------- //
                TcpClient client = new TcpClient(serverIP, port);
                Console.Write("\n>");
                // --------------- Read user input --------------- //
                userInput = Console.ReadLine();
                RetrieveKey(firebaseClient, userInput);
                // Convert to bytes
                byteCount = Encoding.UTF8.GetByteCount(userInput);
                byte[] sendData = new byte[byteCount];
                sendData = Encoding.UTF8.GetBytes(userInput);
                byte[] input = sendData;
                // --------------- Hash the message --------------- //
                uint inputByteLen = Convert.ToUInt32(userInput.Length);
                const uint outputByteLen = 10;
                byte[] output = new byte[outputByteLen];
                // use shake
                FIPS202_SHAKE128(input, inputByteLen, output, outputByteLen);
                // --------------- Combine normal + Hashed message --------------- //
                byte[] combinedMessage = new byte[sendData.Length + 1 + output.Length];
                System.Buffer.BlockCopy(sendData, 0, combinedMessage, 0, sendData.Length);
                System.Buffer.BlockCopy(output, 0, combinedMessage, sendData.Length + 1, output.Length);
                // --------------- Encrypt message --------------- //
                byte[] cipher = new byte[combinedMessage.Length + XSalsa20Poly1305.TagLength];
                XSalsa20Poly1305 xSalsa20Poly1305 = new XSalsa20Poly1305(firstkey);
                xSalsa20Poly1305.Encrypt(cipher, combinedMessage, nonce);
                // --------------- Send message --------------- //
                NetworkStream stream = client.GetStream();
                stream.Write(cipher, 0, cipher.Length);
                stream.Close();
                Console.WriteLine("You entered '{0}'", userInput);
                Console.WriteLine("You sent '{0}'", ByteArrayToString(cipher));
                client.Close();
            } while (userInput != "stop");
            

        }
    }
}
