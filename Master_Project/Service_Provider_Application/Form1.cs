﻿using System;
using System.Timers;
using System.Runtime.CompilerServices;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices; // nodig voor c++ dll
using NaCl; // for salsa
using System.Windows.Forms;
using Newtonsoft.Json;
using FireSharp.Config;
using FireSharp.Interfaces;
using FireSharp.Response;

namespace Service_Provider_Application
{
    public partial class Form1 : Form
    {
        private static System.Timers.Timer timer;
        private static NetworkStream stream;
        private static Stopwatch stopwatch = new Stopwatch();
        private static byte[] nonce = new byte[24];
        // message
        private static byte[] ack = { 0x01 };
        private static byte[] init = { 0x02 };
        private static byte[] msgbyte = { 0x03 };
        private static byte[] lastMessage = new byte[64];
        private static Data[] clockArr;
        private static byte[] key;
        private static byte[] NewKeyBase;
        private static byte[] r1;
        private static byte[] t1 = new byte[24];
        private static bool sent = false;

        [DllImport("D:\\Unief\\1MaIndi\\Master Proef\\projects\\Creating_DLL_for_c#\\Keccak_readable_and_compact.dll", CallingConvention = CallingConvention.Cdecl)]
        static extern void Keccak(uint rate, uint capacity, byte[] input, ulong inputByteLen, char delimitedSuffix, byte[] output, ulong outputByteLen);
        [DllImport("D:\\Unief\\1MaIndi\\Master Proef\\projects\\Creating_DLL_for_c#\\Keccak_readable_and_compact.dll", CallingConvention = CallingConvention.Cdecl)]
        static extern void FIPS202_SHAKE128(byte[] input, uint inputByteLen, byte[] output, uint outputByteLen);
        [DllImport("D:\\Unief\\1MaIndi\\Master Proef\\projects\\Creating_DLL_for_c#\\Keccak_readable_and_compact.dll", CallingConvention = CallingConvention.Cdecl)]
        static extern void FIPS202_SHAKE256(byte[] input, uint inputByteLen, byte[] output, uint outputByteLen);
        [DllImport("D:\\Unief\\1MaIndi\\Master Proef\\projects\\Creating_DLL_for_c#\\Keccak_readable_and_compact.dll", CallingConvention = CallingConvention.Cdecl)]
        static extern void FIPS202_SHA3_224(byte[] input, uint inputByteLen, byte[] output);
        [DllImport("D:\\Unief\\1MaIndi\\Master Proef\\projects\\Creating_DLL_for_c#\\Keccak_readable_and_compact.dll", CallingConvention = CallingConvention.Cdecl)]
        static extern void FIPS202_SHA3_256(byte[] input, uint inputByteLen, byte[] output);
        [DllImport("D:\\Unief\\1MaIndi\\Master Proef\\projects\\Creating_DLL_for_c#\\Keccak_readable_and_compact.dll", CallingConvention = CallingConvention.Cdecl)]
        static extern void FIPS202_SHA3_384(byte[] input, uint inputByteLen, byte[] output);
        [DllImport("D:\\Unief\\1MaIndi\\Master Proef\\projects\\Creating_DLL_for_c#\\Keccak_readable_and_compact.dll", CallingConvention = CallingConvention.Cdecl)]
        static extern void FIPS202_SHA3_512(byte[] input, uint inputByteLen, byte[] output);

        private static void SetTimer()
        {
            // Create a timer with a two second interval.
            timer = new System.Timers.Timer(5000);
            // Hook up the Elapsed event for the timer. 
            timer.Elapsed += OnTimedEvent;
            timer.AutoReset = true;
            timer.Enabled = true;
        }
        private static void OnTimedEvent(Object source, ElapsedEventArgs e)
        {
            stream.Close();
        }
        public static string ByteArrayToString(byte[] ba)
        {
            StringBuilder hex = new StringBuilder(ba.Length * 2);
            foreach (byte b in ba)
                hex.AppendFormat("{0:x2}", b);
            return hex.ToString();
        }
        //------------------------Functions to receive msg----------------------------------------------------//
        public static byte[][] Separate(byte[] source, byte[] separator)
        {
            var Parts = new List<byte[]>();
            var Index = 0;
            byte[] Part;
            for (var i = 0; i < source.Length; ++i)
            {
                if (Equals(source, separator, i))
                {

                    Part = new byte[i - Index];
                    Array.Copy(source, Index, Part, 0, Part.Length);
                    Parts.Add(Part);
                    Index = i + separator.Length;
                    i += separator.Length - 1;
                }
            }
            Part = new byte[source.Length - Index];
            Array.Copy(source, Index, Part, 0, Part.Length);
            Parts.Add(Part);
            return Parts.ToArray();
        }
        public static bool Equals(byte[] source, byte[] separator, int index)
        {
            for (int i = 0; i < separator.Length; ++i)
                if (index + i >= source.Length || source[index + i] != separator[i])
                    return false;
            return true;
        }
        // remove tailing zeroes
        public static byte[] Compact(byte[] packet)
        {
            var i = packet.Length - 1;
            while (packet[i] == 0)
            {
                --i;
            }
            var temp = new byte[i + 1];
            Array.Copy(packet, temp, i + 1);
            return temp;
        }
        public async Task HandleMessage(byte[] encryptedClockMessage, int index)
        {
            // Decrypt clockMessage
            byte[] clockMessage = new byte[encryptedClockMessage.Length];
            XSalsa20 xSalsa20 = new XSalsa20(clockArr[index].Key);
            xSalsa20.Transform(clockMessage, encryptedClockMessage, nonce); // nonce already saved in auth
            Console.WriteLine("You decrypted:'{0}'", Encoding.UTF8.GetString(clockMessage, 0, clockMessage.Length));
            byte[] type = new byte[1];
            Array.Copy(clockMessage, 0, type, 0, 1);
            string msg = "";
            if (type.SequenceEqual(ack)) // 0x01
            {
                // the message has the form ack|ClockMessage
                byte[] ackMessage = new byte[clockMessage.Length - 1];
                Array.Copy(clockMessage, 1, ackMessage, 0, clockMessage.Length - 1);
                msg = Encoding.UTF8.GetString(ackMessage, 0, ackMessage.Length);
                Console.WriteLine("Ack reveived with message:'{0}'", msg);
                GenerateNewKey(NewKeyBase, index); //normal absorb only the decrypted message
                await UpdateData(clockArr[index]);
            }
            else if (type.SequenceEqual(init)) // 0x02
            {
                // the message has the form init|R2||M3
                try
                {
                    byte[] r2 = new byte[32];
                    Array.Copy(clockMessage, 1, r2, 0, 32);
                    byte[] m3 = new byte[32];
                    Array.Copy(clockMessage, 33, m3, 0, 32);
                    // Verify m3
                    bool verify = VerifySharedKey(m3, r1, r2, t1, nonce, index);
                    // generate new key with firstKey, r1 and r2
                    byte[][] combine3 = new byte[3][];
                    combine3[0] = clockArr[index].FirstKey;
                    combine3[1] = r1;
                    combine3[2] = r2;
                    clockArr[index].RawSharedKey = CombineVariablesBytes(combine3);
                    await UpdateData(clockArr[index]);
                }

                catch (Exception ex)
                {
                    Console.WriteLine("Something went wrong while absorbing the initiation response");
                    Console.WriteLine("Error: " + ex.ToString());
                }
            }
            else // actionMessage
            // the message has the form messagebyte|message
            {
                byte[] actionMessage = new byte[clockMessage.Length - 1];
                Array.Copy(clockMessage, 1, actionMessage, 0, clockMessage.Length - 1);
                msg = Encoding.UTF8.GetString(actionMessage, 0, actionMessage.Length);
                Console.WriteLine("ActionMessage reveived with message:'{0}'", msg);
                GenerateNewKey(NewKeyBase, index);
                await UpdateData(clockArr[index]);
                await SendAck("Message succesfully received", index);
            }
        }
        public bool VerifySharedKey(byte[] m3, byte[] r1, byte[] r2, byte[] t1, byte[] t2,int index)
        {
            // Generate sharedKey from all R's and T's
            byte[][] combine4 = new byte[4][];
            combine4[0] = r1;
            combine4[1] = r2;
            combine4[2] = t1;
            combine4[3] = t2;
            byte[] rawServerK = CombineVariablesBytes(combine4);
            uint inputByteLen = Convert.ToUInt32(rawServerK.Length);
            const uint outputByteLen = 32;
            byte[] serverK = new byte[outputByteLen];
            FIPS202_SHAKE128(rawServerK, inputByteLen, serverK, outputByteLen);
            // Generate M3 -------------- NOG STEEDS FIRST KEY
            byte[][] combine2 = new byte[2][];
            combine2[0] = serverK;
            combine2[1] = clockArr[index].FirstKey;
            byte[] rawM3 = CombineVariablesBytes(combine2);
            inputByteLen = Convert.ToUInt32(rawM3.Length);
            byte[] replicaM3 = new byte[outputByteLen];
            FIPS202_SHAKE128(rawM3, inputByteLen, replicaM3, outputByteLen);
            if (m3.SequenceEqual(replicaM3)) // compare with auth
            {
                byte[][] combine3 = new byte[3][];
                combine3[0] = clockArr[index].FirstKey; 
                combine3[1] = r1; 
                combine3[2] = r2; 
                NewKeyBase = CombineVariablesBytes(combine3);
                return true;
            }
            else
            {
                ResponceTxtBox.AppendText("ERROR: Shared key not the same");
                ResponceTxtBox.AppendText(Environment.NewLine);
                return false;
            }
        }
        public async Task SendInit(int index)
        {
            // create the new ServerNonce T1
            TimeSpan ts = stopwatch.Elapsed;
            string serverNonce = (ts.Seconds).ToString();
            t1 = new byte[24];
            System.Buffer.BlockCopy(Encoding.UTF8.GetBytes(serverNonce), 0, t1, 0, Encoding.UTF8.GetBytes(serverNonce).Length);
            // generate R1 --> 32 byte
            Random rnd = new Random();
            r1 = new Byte[32];
            rnd.NextBytes(r1);
            // Combine Init byte + r1 + t1 to create the rawServerMessage
            byte[][] combine2 = new byte[2][];
            combine2[0] = init;  // 1 byte
            combine2[1] = r1; // 32 bytes
            byte[] rawM1 = CombineVariablesBytes(combine2);
            // Encrypt to get the ServerMessage
            byte[] m1 = new byte[rawM1.Count()];
            XSalsa20 xSalsa20 = new XSalsa20(clockArr[index].Key);
            xSalsa20.Transform(m1, rawM1, t1);
            // set length
            byte[] messageLength = new byte[2];
            System.Buffer.BlockCopy(BitConverter.GetBytes(m1.Length),0,messageLength,0,2);
            // Add nonce + messageLength + ServerMessage
            byte[][] combine3 = new byte[3][];
            combine3[0] = t1; // 24bytes
            combine3[1] = messageLength; // 2bytes
            combine3[2] = m1; // message length bytes
            byte[] fullInitMessage = CombineVariablesBytes(combine3);
            // Add the key to create authentication
            combine2 = new byte[2][];
            combine2[0] = clockArr[index].Key;
            combine2[1] = fullInitMessage;
            byte[] rawAuth = CombineVariablesBytes(combine2);
            // hash
            uint inputByteLen = Convert.ToUInt32(rawAuth.Length);
            const uint outputByteLen = 32;
            byte[] auth = new byte[outputByteLen];
            FIPS202_SHAKE128(rawAuth, inputByteLen, auth, outputByteLen);
            // combine ClockMessage with Authentication
            combine2 = new byte[2][];
            combine2[0] = fullInitMessage;
            combine2[1] = auth; // 32 bytes
            byte[] completeInitMessage = CombineVariablesBytes(combine2);
            try
            {
                // send
                stream.Write(completeInitMessage, 0, completeInitMessage.Length);
                //GenerateNewKey(completeInitMessage, index);
                clockArr[index].Initiated = true;
                await UpdateData(clockArr[index]);
                ResponceTxtBox.AppendText("InitMessage sent to " + clockArr[index].Name + " with ID: " + clockArr[index].Id);
                ResponceTxtBox.AppendText(Environment.NewLine);
            }
            catch
            {
                ResponceTxtBox.AppendText("Failed to send InitMessage");
                ResponceTxtBox.AppendText(Environment.NewLine);
            }
        }
        public async Task SendAck(string statusMessage, int index)
        {
            // convert statusMessage to bytes
            byte[] byteStatusMessage = new byte[Encoding.UTF8.GetByteCount(statusMessage)];
            byteStatusMessage = Encoding.UTF8.GetBytes(statusMessage);
            // create the new ServerNonce
            TimeSpan ts = stopwatch.Elapsed;
            string serverNonce = (ts.Seconds).ToString();
            byte[] byteServerNonce = new byte[24];
            System.Buffer.BlockCopy(Encoding.UTF8.GetBytes(serverNonce), 0, byteServerNonce, 0, Encoding.UTF8.GetBytes(serverNonce).Length);
            // Combine Ack byte + statusMessage to create the rawServerMessage
            byte[][] combine2 = new byte[2][];
            combine2[0] = ack;  // 1 byte
            combine2[1] = byteStatusMessage; // 32bytes
            byte[] rawServerMessage = CombineVariablesBytes(combine2);
            // Encrypt to get the ServerMessage
            byte[] serverMessage = new byte[rawServerMessage.Length];
            XSalsa20 xSalsa20 = new XSalsa20(clockArr[index].Key);
            xSalsa20.Transform(serverMessage, rawServerMessage, byteServerNonce);
            // set length
            byte[] messageLength = new byte[2];
            System.Buffer.BlockCopy(BitConverter.GetBytes(serverMessage.Length), 0, messageLength, 0, 2);
            // Add nonce + messageLength + ServerMessage
            byte[][] combine3 = new byte[3][];
            combine3[0] = byteServerNonce;
            combine3[1] = messageLength;
            combine3[2] = serverMessage;
            byte[] fullServerMessage = CombineVariablesBytes(combine3);
            NewKeyBase = fullServerMessage;
            // Add the key to create authentication
            combine2 = new byte[2][];
            combine2[0] = clockArr[index].Key;
            combine2[1] = fullServerMessage;
            byte[] rawAuth = CombineVariablesBytes(combine2);
            // hash
            uint inputByteLen = Convert.ToUInt32(rawAuth.Length);
            const uint outputByteLen = 32;
            byte[] auth = new byte[outputByteLen];
            FIPS202_SHAKE128(rawAuth, inputByteLen, auth, outputByteLen);
            // combine ServerMessage with Authentication
            combine2 = new byte[2][];
            combine2[0] = fullServerMessage;
            combine2[1] = auth;
            byte[] completeServerMessage = CombineVariablesBytes(combine2);
            try
            {
                // send
                stream.Write(completeServerMessage, 0, completeServerMessage.Length);
                GenerateNewKey(NewKeyBase, index);
                await UpdateData(clockArr[index]);
                ResponceTxtBox.AppendText("AckMessage with message: '"+ statusMessage + "' sent to " + clockArr[index].Name + " with ID: " + clockArr[index].Id);
                ResponceTxtBox.AppendText(Environment.NewLine);
            }
            catch
            {
                ResponceTxtBox.AppendText("Failed to send AckMessage");
                ResponceTxtBox.AppendText(Environment.NewLine);
            }
        }
        public async Task SendMessage(string statusMessage, int index)
        {
            // convert statusMessage to bytes
            byte[] byteStatusMessage = new byte[Encoding.UTF8.GetByteCount(statusMessage)];
            byteStatusMessage = Encoding.UTF8.GetBytes(statusMessage);
            // create the new ServerNonce
            TimeSpan ts = stopwatch.Elapsed;
            string serverNonce = (ts.Seconds).ToString();
            byte[] byteServerNonce = new byte[24];
            System.Buffer.BlockCopy(Encoding.UTF8.GetBytes(serverNonce), 0, byteServerNonce, 0, Encoding.UTF8.GetBytes(serverNonce).Length);
            // Combine msg byte + statusMessage to create the rawServerMessage
            byte[][] combine2 = new byte[2][];
            combine2[0] = msgbyte;  // 1 byte
            combine2[1] = byteStatusMessage; // 32bytes
            byte[] rawServerMessage = CombineVariablesBytes(combine2);
            // Encrypt to get the ServerMessage
            byte[] serverMessage = new byte[rawServerMessage.Length];
            XSalsa20 xSalsa20 = new XSalsa20(clockArr[index].Key);
            xSalsa20.Transform(serverMessage, rawServerMessage, byteServerNonce);
            // set length
            byte[] messageLength = new byte[2];
            System.Buffer.BlockCopy(BitConverter.GetBytes(serverMessage.Length),0,messageLength,0,2);
            // Add nonce + messageLength + ServerMessage
            byte[][] combine3 = new byte[3][];
            combine3[0] = byteServerNonce;
            combine3[1] = messageLength;
            combine3[2] = serverMessage;
            byte[] fullServerMessage = CombineVariablesBytes(combine3);
            NewKeyBase = fullServerMessage;
            // Add the key to create authentication
            combine2 = new byte[2][];
            combine2[0] = clockArr[index].Key;
            combine2[1] = fullServerMessage;
            byte[] rawAuth = CombineVariablesBytes(combine2);
            // hash
            uint inputByteLen = Convert.ToUInt32(rawAuth.Length);
            const uint outputByteLen = 32;
            byte[] auth = new byte[outputByteLen];
            FIPS202_SHAKE128(rawAuth, inputByteLen, auth, outputByteLen);
            // combine ClockMessage with Authentication
            combine2 = new byte[2][];
            combine2[0] = fullServerMessage;
            combine2[1] = auth;
            byte[] completeServerMessage = CombineVariablesBytes(combine2);
            try
            {
                // send
                stream.Write(completeServerMessage, 0, completeServerMessage.Length);
                if(clockArr[index].CompleteInitiation)
                {
                    GenerateNewKey(NewKeyBase, index);
                    await UpdateData(clockArr[index]);
                }
                else
                {
                    ResponceTxtBox.AppendText("Iniiation not completed for " + clockArr[index].Name + " with ID: " + clockArr[index].Id);
                    ResponceTxtBox.AppendText(Environment.NewLine);
                    ResponceTxtBox.AppendText("message not absorbed");
                    ResponceTxtBox.AppendText(Environment.NewLine);
                }
                ResponceTxtBox.AppendText("ActionMessage with message: '" + statusMessage + "' sent to " + clockArr[index].Name + " with ID: " + clockArr[index].Id);
                ResponceTxtBox.AppendText(Environment.NewLine);
            }
            catch
            {
                ResponceTxtBox.AppendText("Failed to send ActionMessage");
                ResponceTxtBox.AppendText(Environment.NewLine);
            }
        }
        public bool AuthenticateMessage(byte[] receivedBuffer, out byte[] encryptedClockMessage, byte[] currentkey)
        {
            // remove tailing zeros to only obtain the CompleteMessage
            byte[] completeMessage = Compact(receivedBuffer);
            Console.WriteLine("You received:'{0}'", ByteArrayToString(completeMessage));
            // assigne nonce and serverMessage
            Array.Copy(completeMessage,0,nonce,0,24);
            byte[] convertLength = new byte[4];
            byte[] length = new byte[2];
            Array.Copy(completeMessage, 24, convertLength, 0, 2);
            Array.Copy(completeMessage, 24, length, 0, 2);
            encryptedClockMessage = new byte[BitConverter.ToInt32(convertLength, 0)];
            Array.Copy(completeMessage, 26, encryptedClockMessage, 0, BitConverter.ToInt32(convertLength, 0));
            byte[] auth = new byte[32];
            Array.Copy(completeMessage, 26 + BitConverter.ToInt32(convertLength, 0), auth, 0, 32);
            // Hash the given key with full Servermessag
            byte[][] combine4 = new byte[4][];
            combine4[0] = currentkey; // key
            combine4[1] = nonce;  // nonce
            combine4[2] = length; // length
            combine4[3] = encryptedClockMessage; // clockmessage
            byte[] input = CombineVariablesBytes(combine4);
            uint inputByteLen = Convert.ToUInt32(input.Length);
            const uint outputByteLen = 32;
            byte[] output = new byte[outputByteLen];
            FIPS202_SHAKE128(input, inputByteLen, output, outputByteLen);
            if (auth.SequenceEqual(output)) // compare with auth
            {
                byte[][] combine3 = new byte[3][];
                combine3[0] = nonce;  // nonce
                combine3[1] = length; // length
                combine3[2] = encryptedClockMessage; // clockmessage
                NewKeyBase = CombineVariablesBytes(combine3);
                return true;
            }
            else
            {
                Console.WriteLine("ERROR: Authentication failed");
                return false;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <param name="index"></param>
        /// <returns></returns>
        public bool GenerateNewKey(byte[] input, int index)
        {
            try
            {
                // Generate new key using the last key and the RawClockMessage
                byte[][] combine2 = new byte[2][];
                combine2[0] = clockArr[index].Key;
                combine2[1] = input;
                byte[] rawKey = CombineVariablesBytes(combine2);
                uint inputByteLen = Convert.ToUInt32(input.Length);
                const uint outputByteLen2 = 32;
                byte[] output = new byte[outputByteLen2];
                FIPS202_SHAKE128(input, inputByteLen, output, outputByteLen2);
                // Update variables
                clockArr[index].PrevKey = key;
                clockArr[index].Key = output;
                ResponceTxtBox.Text += "NewKey for Clock " + clockArr[index].Name + " with ID: " + clockArr[index].Id;
                ResponceTxtBox.AppendText(Environment.NewLine);
                return true;
            }
            catch
            {
                return false;
            }
        }
        public async Task UpdateData(Data clock)
        {
            try
            {
                FirebaseResponse response = await client.SetAsync("information/" + clock.Id, clock);
                Data result = response.ResultAs<Data>();
                ResponceTxtBox.Text += "Data updated for Clock " + clock.Name + " with ID: " + clock.Id;
                ResponceTxtBox.AppendText(Environment.NewLine);
                await ReceiveData();

            }
            catch (Exception ex)
            {
                ResponceTxtBox.Text += "Failed to update data for " + clock.Name + " with ID: " + clock.Id +" with exception: " + ex.ToString();
                ResponceTxtBox.AppendText(Environment.NewLine);
            }
        }
        public async Task ReceiveData()
        {
            FirebaseResponse response = await client.GetAsync("information");
            /// Te kleine lijst wordt via eeen Json formaat doorgestuurd
            try
            {
                IDictionary<string, Data> data = JsonConvert.DeserializeObject<IDictionary<string, Data>>(response.Body);
                string[] keys = data.Keys.ToArray();
                clockArr = data.Values.ToArray();
            }
            /// grotere lijsten kunnen door
            catch
            {
                clockArr = response.ResultAs<Data[]>();
                clockArr = clockArr.Where(c => c != null).ToArray();
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="variables"></param>
        /// <returns></returns>
        public byte[] CombineVariablesBytes(byte[][] variables)
        {
            int bytelength = 0;
            for (int i = 0; i < variables.Length; i++)
            {
                bytelength += variables[i].Length;
            }
            byte[] combinedResult = new byte[bytelength];
            bytelength = 0;
            for (int i = 0; i < variables.Length; i++)
            {
                System.Buffer.BlockCopy(variables[i], 0, combinedResult, bytelength, variables[i].Length);
                System.Buffer.BlockCopy(variables[i], 0, combinedResult, bytelength, variables[i].Length);
                bytelength += variables[i].Length;
            }

            return combinedResult;
        }
        //----------------------------------------------------------------------------//


        /// sockets
        string serverIP = "localhost";
        int port;
        /// keccak
        string userInput;
        IFirebaseConfig config = new FirebaseConfig
        {
            AuthSecret = "WMZC9oaHVt7T9lwtyutMrhDrREgyZ3mDDmZl0HDW",
            BasePath = "https://ctry-ee061.firebaseio.com/"
        };
        IFirebaseClient client;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            stopwatch.Start();
            client = new FireSharp.FirebaseClient(config);
            if (client != null)
            {
                //MessageBox.Show("Connection Established");
            }
            else { MessageBox.Show("Connection NOT Established!"); }
        }
        private void ClockCheckBoxList_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }

        private async void RefreshBtn_Click(object sender, EventArgs e)
        {
            ClockCheckBoxList.Items.Clear();
            await ReceiveData();
            string[] names = new string[clockArr.Length];
            for (int i = 0; i < clockArr.Length; i++)
            {
                if (clockArr[i].Name == null)
                {
                    names[i] = "No Name - Id: " + clockArr[i].Id + " - Port: " + clockArr[i].Port;
                }
                else
                {
                    names[i] = clockArr[i].Name + " - Id: " + clockArr[i].Id + " - Port: " + clockArr[i].Port;
                }
                
            }
            ClockCheckBoxList.Items.Clear();
            ClockCheckBoxList.Items.AddRange(names);
        }

        private async void SendBtn_Click(object sender, EventArgs e)
        {
            ResponceTxtBox.Clear();
            if (ClockCheckBoxList.CheckedItems.Count == 0 )
            {
                ResponceTxtBox.Text = "No clock selected! Send Failed";
            }
            else
            {
                IPAddress ip = Dns.GetHostEntry("localhost").AddressList[0];
                // pick the selected clocks
                int count = 0;
                for (int i = 0; i < ClockCheckBoxList.Items.Count; i++)
                {
                    if (ClockCheckBoxList.GetItemCheckState(i) == CheckState.Checked)
                    {
                            // start send sequence
                            port = clockArr[i].Port;
                            key = clockArr[i].Key;
                            try
                            {
                                // // --------------- start TCP client --------------- //
                                TcpClient client = new TcpClient(serverIP, port);
                                stream = client.GetStream();
                                // convert to bytes
                                userInput = MessageTxtBox.Text;
                                await SendMessage(userInput, i);
                                sent = true;
                                if(clockArr[i].CompleteInitiation)
                                {
                                SetTimer();
                                //----------------- listening ------------------//
                                bool auth = false;
                                while (!auth)
                                {
                                    // bytes that are going to be received by the server
                                    byte[] receivedBuffer = new byte[256];
                                    // read the network stream, in the receivedBuffer, starting on the 0'th byte
                                    stream.Read(receivedBuffer, 0, receivedBuffer.Length);
                                    timer.Dispose();
                                    byte[] encryptedClockMessage;
                                    auth = AuthenticateMessage(receivedBuffer, out encryptedClockMessage, clockArr[i].Key);
                                    if (auth)
                                    {
                                        await HandleMessage(encryptedClockMessage, i);
                                        ResponceTxtBox.AppendText("Received Ack from Clock " + clockArr[i].Name + " with ID: " + clockArr[i].Id);
                                        ResponceTxtBox.AppendText(Environment.NewLine);

                                    }
                                    else
                                    {
                                        ResponceTxtBox.AppendText("Message discarded ");
                                        ResponceTxtBox.AppendText(Environment.NewLine);
                                    }
                                }
                                    //----------
                                }
                                client.Close();
                                stream.Close();
                            }
                            catch (Exception ex)
                            {
                                if (sent == true)
                                {
                                ResponceTxtBox.AppendText("Error: No ack received from clock " + clockArr[i].Name + " with ID: " + clockArr[i].Id + ".  One of the following went wrong: ");
                                ResponceTxtBox.AppendText(Environment.NewLine);
                                ResponceTxtBox.AppendText("Wrong key, Clock not found or Clock offline."); ResponceTxtBox.AppendText(Environment.NewLine);
                                }
                                else
                                {
                                    ResponceTxtBox.AppendText("Clock " + clockArr[i].Name + " with ID: " + clockArr[i].Id + " is not found or offline. Error  : " + ex.ToString());
                                    ResponceTxtBox.AppendText(Environment.NewLine);
                                }

                            }
                            count++;
                            sent = false;
                            ResponceTxtBox.AppendText(Environment.NewLine);
                    }
                }
            }
            

        }

        private async void ResetBtn_Click(object sender, EventArgs e)
        {
            ResponceTxtBox.Clear();
            if (ClockCheckBoxList.CheckedItems.Count == 0)
            {
                ResponceTxtBox.Text = "No clock selected! Reset Failed";
            }
            else
            {
                int count = 0;
                for (int i = 0; i < ClockCheckBoxList.Items.Count; i++)
                {
                    if (ClockCheckBoxList.GetItemCheckState(i) == CheckState.Checked)
                    {
                        // extract the data
                        var data = new Data()
                        {
                            Id = clockArr[i].Id,
                            Name = clockArr[i].Name,
                            FirstKey = new byte[]
                            {
                            0x1b, 0x27, 0x55, 0x64, 0x73, 0xe9, 0x85,
                            0xd4, 0x62, 0xcd, 0x51, 0x19, 0x7a, 0x9a,
                            0x46, 0xc7, 0x60, 0x09, 0x54, 0x9e, 0xac,
                            0x64, 0x74, 0xf2, 0x06, 0xc4, 0xee, 0x08,
                            0x44, 0xf6, 0x83, 0x89
                            },
                            Key = clockArr[i].FirstKey,
                            Port = clockArr[i].Port,
                            IP = "127.0.0.1",
                            CompleteInitiation = false,
                            Initiated = false,
                            RawSharedKey = new byte[32]
                        }; 
                        FirebaseResponse response = await client.SetAsync("information/" + data.Id, data);
                        Data result = response.ResultAs<Data>();
                        count++;
                        ResponceTxtBox.Text += "Data reset for Clock " + data.Name + " with ID: " + data.Id;
                        ResponceTxtBox.AppendText(Environment.NewLine);
                    }
                }
                RefreshBtn_Click(sender, e);
            }
        }

        private async void InitBtn_Click(object sender, EventArgs e)
        {
            ResponceTxtBox.Clear();
            if (ClockCheckBoxList.CheckedItems.Count == 0)
            {
                ResponceTxtBox.Text = "No clock selected! Initiation Failed";
            }
            else
            {
                IPAddress ip = Dns.GetHostEntry("localhost").AddressList[0];
                int count = 0;
                for (int i = 0; i < ClockCheckBoxList.Items.Count; i++)
                {
                    if (ClockCheckBoxList.GetItemCheckState(i) == CheckState.Checked)
                    {
                        if (!clockArr[i].CompleteInitiation)
                        {
                            // start send sequence
                            port = clockArr[i].Port;
                            key = clockArr[i].Key;
                            try
                            {
                                // // --------------- start TCP client --------------- //
                                TcpClient client = new TcpClient(serverIP, port);
                                stream = client.GetStream();
                                // convert to bytes
                                userInput = MessageTxtBox.Text;
                                await SendInit(i);
                                sent = true;
                                SetTimer();
                                //----------------- listening ------------------//
                                bool auth = false;
                                // bytes that are going to be received by the server
                                byte[] receivedBuffer = new byte[256];
                                // read the network stream, in the receivedBuffer, starting on the 0'th byte
                                stream.Read(receivedBuffer, 0, receivedBuffer.Length);
                                timer.Dispose();
                                byte[] encryptedClockMessage;
                                auth = AuthenticateMessage(receivedBuffer, out encryptedClockMessage, clockArr[i].Key);
                                if (auth)
                                {
                                    await HandleMessage(encryptedClockMessage, i);
                                    ResponceTxtBox.AppendText("Received Init Response from Clock " + clockArr[i].Name + " with ID: " + clockArr[i].Id);
                                    ResponceTxtBox.AppendText(Environment.NewLine);
                                }
                                else
                                {
                                    ResponceTxtBox.AppendText("Message discarded. Initiation Failed");
                                    ResponceTxtBox.AppendText(Environment.NewLine);
                                }
                                //----------
                                client.Close();
                                stream.Close();
                            }
                            catch (Exception ex)
                            {
                                if (sent == true)
                                {
                                    ResponceTxtBox.AppendText("Error: No ack received from clock " + clockArr[i].Name + " with ID: " + clockArr[i].Id + ".  One of the following went wrong: ");
                                    ResponceTxtBox.AppendText(Environment.NewLine);
                                    ResponceTxtBox.AppendText("Wrong key, Clock not found or Clock offline.");
                                    ResponceTxtBox.AppendText(Environment.NewLine);
                                }
                                else
                                {
                                    ResponceTxtBox.AppendText("Send failed for Clock " + clockArr[i].Name + " with ID: " + clockArr[i].Id + " with exception: " + ex.ToString());
                                    ResponceTxtBox.AppendText(Environment.NewLine);
                                }

                            }
                            count++;
                            sent = false;
                        }
                        else
                        {
                            ResponceTxtBox.AppendText("Clock " + clockArr[i].Name + " with ID: " + clockArr[i].Id + " has already been initiated.");
                            ResponceTxtBox.AppendText(Environment.NewLine);
                        }

                    }
                }
            }
        }

        private async void AckBtn_Click(object sender, EventArgs e)
        {
            ResponceTxtBox.Clear();
            if (ClockCheckBoxList.CheckedItems.Count == 0)
            {
                ResponceTxtBox.Text = "No clock selected! Initiation Failed";
            }
            else
            {
                IPAddress ip = Dns.GetHostEntry("localhost").AddressList[0];
                int count = 0;
                for (int i = 0; i < ClockCheckBoxList.Items.Count; i++)
                {
                    if (ClockCheckBoxList.GetItemCheckState(i) == CheckState.Checked)
                    {
                        if (clockArr[i].Initiated)
                        {
                            if (!clockArr[i].CompleteInitiation)
                            {
                                // start send sequence
                                port = clockArr[i].Port;
                                key = clockArr[i].Key;
                                try
                                {
                                    // // --------------- start TCP client --------------- //
                                    TcpClient client = new TcpClient(serverIP, port);
                                    stream = client.GetStream();
                                    // convert to bytes
                                    userInput = MessageTxtBox.Text;
                                    GenerateNewKey(clockArr[i].RawSharedKey, i);
                                    clockArr[i].CompleteInitiation = true;
                                    await UpdateData(clockArr[i]);
                                    await SendAck("Init Response received", i);
                                    ResponceTxtBox.AppendText("Ack sent to Clock " + clockArr[i].Name + " with ID: " + clockArr[i].Id);
                                    ResponceTxtBox.AppendText(Environment.NewLine);
                                    //----------
                                    client.Close();
                                    stream.Close();
                                }
                                catch (Exception ex)
                                {
                                    ResponceTxtBox.AppendText("Error: " + ex.ToString());
                                    ResponceTxtBox.AppendText(Environment.NewLine);
                                }
                                count++;
                            }
                            else
                            {
                                ResponceTxtBox.AppendText("Clock " + clockArr[i].Name + " with ID: " + clockArr[i].Id + " has already completed Initiation.");
                                ResponceTxtBox.AppendText(Environment.NewLine);
                            }
                        }
                        else
                        {
                            ResponceTxtBox.AppendText("Clock " + clockArr[i].Name + " with ID: " + clockArr[i].Id + " hasn't been Initiated yet.");
                            ResponceTxtBox.AppendText(Environment.NewLine);
                        }
                        

                    }
                }
            }
        }
    }
}
