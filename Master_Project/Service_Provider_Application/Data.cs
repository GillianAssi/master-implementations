﻿namespace Service_Provider_Application
{
    public class Data
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public byte[] Key { get; set; }
        public byte[] FirstKey { get; set; }
        public byte[] PrevKey { get; set; }
        public byte[] Nonce { get; set; }
        public byte[] RawSharedKey { get; set; }
        public int Port { get; set; }
        public string IP { get; set; }
        public bool Initiated { get; set; }
        public bool CompleteInitiation { get; set; }
        
    }
}