﻿namespace FirebaseConsole
{
    internal class Data
    {
        public object Id { get; set; }
        public object Name { get; set; }
        public object Age { get; set; }
        public object Adress { get; set; }
    }
}